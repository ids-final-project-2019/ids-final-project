module graphicsGen(col, row, clk, red, green, blue, hex);
	input  [9:0]  col;	//0->639
	input  [8:0]  row;	//0->479
	output [8:0] hex;
	input clk;
	output [3:0] red, green, blue; //4-bit color input
	reg [15:0] count;
	
	//Right now this is off, but is useful for checking clock speeds and such
	assign hex[7:0] = 7'b1111111;
	assign hex[8] = clk;
	
	//instantiating the actual drawing finally
	funLogic log (clk, red, green, blue, row, col);
	
endmodule

module funLogic(clock, r, g, b, row, col);
	input clock;
	input  [9:0]  col;	//0->639
	input  [8:0]  row;	//0->479
	parameter maxRow = 470; //these are set to values -10 to account for block size, to be changed later
	parameter maxCol = 630;
	output [3:0] r, g, b; //self explanatory
	reg [9:0] curX;
	reg [8:0] curY;
	reg yP, xP;
/*
Just let me preface all of this with that coordinates here are dumb
(0,0) is top left corner
(480, 640) is bottom right corner, you can infer the rest
*/
	always @(posedge clock) begin
		//top-bottom logic
		if(xP) begin
			//if going to hit bottom
			if(curY < maxRow) curY = curY + 2'b10;
			//don't
			else xP = 0;
		end 
		else begin
			//if going to hit top
			if(curY > 0) curY = curY - 2'b10;
			//don't
			else xP = 1;
		end
		//left-right logic
		if(yP) begin
			//if going to hit right
			if(curX < maxCol) curX = curX + 2'b10;
			//don't
			else yP = 0;
		end 
		else begin
			//if going to hit left
			if(curX > 0) curX = curX - 2'b10;
			else yP = 1;
		end
	end
	//if the pixel is between the:
	//					top and bottom of the square 				left and right of the square   then draw	or don't
	assign r = ((row > curY && row < curY + 4'd10) && (col > curX && col < curX + 4'd10)) ? 4'b1111 : 4'b0000;
	assign g = r; //these are equal to red for the moment to make it white or black
	assign b = r; //maybe in final we change this up a bit
endmodule

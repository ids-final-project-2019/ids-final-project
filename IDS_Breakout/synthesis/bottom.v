module bottom (ballPosX, ballPosY, clk_in, g_over);
	input [9:0] ballPosX;
	input [8:0] ballPosY;
	input clk_in;
	output reg g_over;

	//if ball reaches height of 11 pixels, game over via state machine (transition condition of g_over). (ball reflects along bottom at 10 pixels, 
	always @(posedge clk_in) begin
		if(ballPosY > 9'd480) g_over <= 1;
		else g_over <= 0; 
	end
endmodule

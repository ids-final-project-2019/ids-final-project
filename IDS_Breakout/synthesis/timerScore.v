//module Lab_7b (reset, clk_in, bt_set, bt_show, bt_reset, hex0, hex1, hex2, hex3, hex4);
//	output [7:0] hex0, hex1, hex3, hex2, hex4;
//	input bt_show, bt_reset, bt_set;
//	input reset;
//	input clk_in;
//	wire clock;
//	wire [19:0] T, newT;
//	
//	stateMachine state(clk_in, reset, T);
//	bestMux muxy (T, bt_set, bt_show, bt_reset, newT);
//	display dis (newT, hex0[6:0], hex1[6:0], hex2[6:0], hex3[6:0], hex4[6:0]);
//	
//	assign hex1[7] = 1;
//	assign hex0[7] = 1;
//	assign hex2[7] = 0;
//	assign hex3[7] = 1;
//	assign hex4[7] = 1;
//	
//endmodule
//
//module bestMux(curT, set, show, reset, out);
//	input [19:0] curT;
//	input set, show;
//	input reset;
//	output reg [19:0] out;
//	reg [19:0] best;
//
//	always @(set or show or reset) begin
//		if(reset) begin
//			best <= 20'b11111111111111111111;
//		end
//		else begin
//			if (set) begin
//				best <= (curT < best && curT >  0) ? curT : best;
//			end
//			if (show || set) begin
//				out <= best;
//			end
//			else begin
//				out <= curT;
//			end
//		end
//	end
//endmodule
//
//module stateMachine(clk, reset, num);
//	input clk, reset;
//	output reg [19:0] num;
//	reg [3:0] p0, p1, p2, p3, p4;
//	
//	always @(posedge clk or posedge reset) begin
//		if(reset) begin
//			num <= 0;
//			p0 <= 0;
//			p1 <= 0;
//			p2 <= 0;
//			p3 <= 0;
//			p4 <= 0;
//		end
//		else begin
//			p0 <= p0 + 1;
//			if(p0 == 4'd10) begin
//				p0 <= 0;
//				p1 <= p1 + 1;
//			end
//			if(p1 == 4'd10) begin
//				p1 <= 0;
//				p2 <= p2 + 1;
//			end
//			if(p2 == 4'd10) begin
//				p2 <= 0;
//				p3 <= p3 + 1;
//			end
//			if(p3 == 4'd10) begin
//				p3 <= 0;
//				p4 <= p4 + 1;
//			end
//			if(p4 == 4'd10) begin
//				p4 <= 0;
//			end
//		end
//		num <= {p4, p3, p2, p1, p0};
//	end
//endmodule
//
//module display(number, hex0, hex1, hex2, hex3, hex4);
//	input [19:0] number;
//	output wire [6:0] hex0, hex1, hex2, hex3, hex4;
//	
//	behavioral inst0(number[3:0], hex0);
//	behavioral inst1(number[7:4], hex1);
//	behavioral inst2(number[11:8], hex2);
//	behavioral inst3(number[15:12], hex3);
//	behavioral inst4(number[19:16], hex4);
//endmodule
//
//module behavioral(bcd, hex);
//	input [3:0] bcd;
//	output [6:0] hex;
//	reg [6:0] hex;
//	
//	always @ (bcd) begin
//		case(bcd)
//			4'd1 : hex <= ~7'b0000110;
//			4'd2 : hex <= ~7'b1011011;
//			4'd3 : hex <= ~7'b1001111;
//			4'd4 : hex <= ~7'b1100110;
//			4'd5 : hex <= ~7'b1101101;
//			4'd6 : hex <= ~7'b1111101;
//			4'd7 : hex <= ~7'b0000111;
//			4'd8 : hex <= ~7'b1111111;
//			4'd9 : hex <= ~7'b1100111;
//			4'd10 : hex <= ~7'b1110111;
//			4'd11 : hex <= ~7'b1111100;
//			4'd12 : hex <= ~7'b0111001;
//			4'd13 : hex <= ~7'b1011110;
//			4'd14 : hex <= ~7'b1111001;
//			4'd0 : hex <= ~7'b0111111;
//			4'd15 : hex <= ~7'b1110001;
//		endcase
//	end
//endmodule

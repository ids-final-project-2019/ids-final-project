module ball (collidingX, collidingY, ballPosX, ballPosY, col, row, color, clk_in, reset, vecX, vecY, posX);
	input collidingX, collidingY;
	input clk_in;
	input reset;
	input  [9:0]  col;	//0->639
	input  [8:0]  row;   //0->479
	input  [2:0] vecX, vecY;
	output [9:0] ballPosX;
	output [8:0] ballPosY;
	output [2:0] color;
	output posX;
	
	position posBoi (collidingX, collidingY, ballPosX, ballPosY, clk_in, reset, vecX, vecY, posX);
	
	drawingBall ballDraw(ballPosX, ballPosY, col, row, color);
	
endmodule

module position (collidingX, collidingY, ballPosX, ballPosY, clk_in, reset, vecX, vecY, posX);
	input clk_in;
	input reset;
	input collidingX, collidingY;
	output reg posX;
	reg posY;
	input [2:0] vecX, vecY;
	parameter maxRow = 470; //these are set to values -10 to account for ball
	parameter maxCol = 630;
	output reg [9:0] ballPosX;
	output reg [8:0] ballPosY;
	
	initial begin //initial movement to the left and up
		posX <= 1'b0;
		posY <= 1'b0;
		ballPosX <= 10'd316;
		ballPosY <= 9'd240;
	end
		
	always @(posedge clk_in) begin
		if(reset) begin
			posX <= 1'b0;
			posY <= 1'b0;
			ballPosX <= 10'd316;
			ballPosY <= 9'd460;
		end
		else begin
			if(posX) begin //if moving in the positive direction, add the motion vector
				if(collidingX || ballPosX > maxCol)
					posX <= 0;
				else ballPosX <= ballPosX + vecX;
			end
			else begin //otherwise subtract it
				if(collidingX || ballPosX < 4'd3)
					posX <= 1;
				else
					ballPosX <= ballPosX - vecX;
			end
			
			if(posY) begin //this is the same thing, add
				if(collidingY)
					posY <= 0;
				else
					ballPosY <= ballPosY + vecY;
			end
			else begin //otherwise subtract
				if(collidingY || ballPosY < 4'd3)
					posY <= 1;
				else
					ballPosY <= ballPosY - vecY;
			end
		end
	end
endmodule

module drawingBall(ballPosX, ballPosY, col, row, color);
	input [9:0] ballPosX;
	input [8:0] ballPosY;
	input  [9:0]  col;	//0->639
	input  [8:0]  row;	//0->479
	output reg [2:0] color;
	
	always @* begin
		color <= (col < ballPosX + 4'd10 && col > ballPosX && row < ballPosY + 4'd10 && row > ballPosY) ? 3'b111 : 3'b000; //10-by-10 square
	end
endmodule
 
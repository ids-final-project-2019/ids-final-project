module blocks (ballPosX, ballPosY, collidingX, collidingY, col, row, color, clk_in, reset, brokeCount);
	input clk_in, reset;
	input [9:0] ballPosX;
	input [8:0] ballPosY;
	input  [9:0]  col;	//0->639
	input  [8:0]  row;	//0->479
	output collidingX, collidingY;
	output [2:0] color;
	wire [2:0] color1, color2, color3, color4, color5;
	output [5:0] brokeCount;
//	output reg [7:0] brokenCount;
	wire [3:0] brokeCount1, brokeCount2, brokeCount3, brokeCount4, brokeCount5;
	
	//for each row we instantiate, it instantiates 8 blocks, getting us the 40 blocks we need
	row row1 (8'd50, ballPosX, ballPosY, collidingX1, collidingY1, col, row, color1, clk_in, reset, brokeCount1); //these instantiate one row each
	row row2 (8'd80, ballPosX, ballPosY, collidingX2, collidingY2, col, row, color2, clk_in, reset, brokeCount2);
	row row3 (8'd110, ballPosX, ballPosY, collidingX3, collidingY3, col, row, color3, clk_in, reset, brokeCount3);
	row row4 (8'd140, ballPosX, ballPosY, collidingX4, collidingY4, col, row, color4, clk_in, reset, brokeCount4);
	row row5 (8'd170, ballPosX, ballPosY, collidingX5, collidingY5, col, row, color5, clk_in, reset, brokeCount5);
	
	//Mass OR-ing again, to see if the ball collides with or the current pixel is inside of the blocks of any of the rows
	assign color = color1 | color2 | color3 | color4 | color5;
	assign collidingX = collidingX1 | collidingX2 | collidingX3 | collidingX4 | collidingX5;
	assign collidingY = collidingY1 | collidingY2 | collidingY3 | collidingY4 | collidingY5;
	assign brokeCount = brokeCount1 + brokeCount2 + brokeCount3 + brokeCount4 + brokeCount5;
	
//	always @(posedge brokeCount[0] or posedge reset) begin
//		if(reset) begin
//			brokenCount = 0;
//		end
//		else begin
//			brokenCount[3:0] = brokenCount[3:0] + 1;
//			if(brokenCount[3:0] == 4'b1010) begin
//				brokenCount[3:0] = 4'b0000;
//				brokenCount[7:4] = brokenCount[7:4] + 1;
//			end
//		end
//	end
	
endmodule

module row (height, ballPosX, ballPosY, collidingX, collidingY, col, row, color, clk_in, reset, brokeCount);
	input clk_in, reset;
	input [9:0] ballPosX;
	input [8:0] ballPosY;
	input  [9:0]  col;	//0->639
	input  [8:0]  row;	//0->479
	input [8:0] height;
	output collidingX, collidingY;
	output [2:0] color;
	wire [2:0] color1, color2, color3, color4, color5, color6, color7, color8;
	output [3:0] brokeCount;
	
	block block1 (height, 10'd6, ballPosX, ballPosY, collidingX1, collidingY1, col, row, color1, clk_in, reset, brokeCount1); //these instantiate one block each
	block block2 (height, 10'd86, ballPosX, ballPosY, collidingX2, collidingY2, col, row, color2, clk_in, reset, brokeCount2);
	block block3 (height, 10'd166, ballPosX, ballPosY, collidingX3, collidingY3, col, row, color3, clk_in, reset, brokeCount3);
	block block4 (height, 10'd246, ballPosX, ballPosY, collidingX4, collidingY4, col, row, color4, clk_in, reset, brokeCount4);
	block block5 (height, 10'd326, ballPosX, ballPosY, collidingX5, collidingY5, col, row, color5, clk_in, reset, brokeCount5);
	block block6 (height, 10'd406, ballPosX, ballPosY, collidingX6, collidingY6, col, row, color6, clk_in, reset, brokeCount6);
	block block7 (height, 10'd486, ballPosX, ballPosY, collidingX7, collidingY7, col, row, color7, clk_in, reset, brokeCount7);
	block block8 (height, 10'd566, ballPosX, ballPosY, collidingX8, collidingY8, col, row, color8, clk_in, reset, brokeCount8);
	
	//we OR together all of the values to get whether or not the pixel is inside of the blocks in the row
	//also to see if it collides with any of the blocks
	assign color = color1 | color2 | color3 | color4 | color5 | color6 | color7 | color8;
	assign collidingX = collidingX1 | collidingX2 | collidingX3 | collidingX4 | collidingX5 | collidingX6 | collidingX7 | collidingX8; 
	assign collidingY = collidingY1 | collidingY2 | collidingY3 | collidingY4 | collidingY5 | collidingY6 | collidingY7 | collidingY8;
	assign brokeCount = brokeCount1 + brokeCount2 + brokeCount3 + brokeCount4 + brokeCount5 + brokeCount6 + brokeCount7 + brokeCount8;
	
endmodule

module block (height, width, ballPosX, ballPosY, collidingX, collidingY, col, row, color, clk_in, reset, broken);
	input clk_in, reset;
	input [9:0] ballPosX;
	input [8:0] ballPosY;
	input  [9:0]  col, width;	//0->639
	input  [8:0]  row, height;	//0->479
	output reg collidingX, collidingY;
	output reg [2:0] color;
	output reg broken;
	
	initial begin
		broken <= 0;
	end
	
	always @* begin
		color <= (col < width + 7'd70 && col > width && row < height + 6'd25 && row > height && ~broken) ? 3'b101 : 3'b000;
	end
	
	always @(posedge clk_in) begin
		if (reset)
			broken <= 0;
		else begin
			if (~broken) begin
				if(ballPosX <  width + 7'd70 && (ballPosX > width - 4'd10 || width == 4'd6) && ballPosY < height + 6'd25 && ballPosY > height - 4'd10) begin //is it inside the box
					if (ballPosX < width - 4'd5 || ballPosX > width + 7'd65) begin //ball is 10 pixels by 10 pixels
	//						-5 instead of +5 to accomodate for pixel drawing of the 10-pixel ball
						collidingX <= 1'd1;
						broken <= 1'd1;
						end
					if (ballPosY < height - 4'd5  || ballPosY > height + 6'd20) begin
	//					'' also -5 on both accounts for 10-pixel ball
						collidingY <= 1'd1;
						broken <= 1'd1;
					end
				end
			end
			else begin
	//			if it doesn't exist, say the ball isn't colliding
				collidingX <= 1'd0;
				collidingY <= 1'd0;
			end
		end
	end
endmodule

module paddle (directionInput, ballPosX, ballPosY, collidingY, col, row, color, clk_in, reset, vecX, vecY, posX, collidingX);
	input [1:0] directionInput; // [1,0] is left, [0,1] is right, anything else is not moving
	input clk_in, reset, posX;
	input [9:0] ballPosX;
	input [8:0] ballPosY;
	input  [9:0]  col;	//0->639
	input  [8:0]  row;	//0->479
	output [2:0] color; //0000 is off, 0001 is red, 0010 is blue, 0100 is green, 1000 is white
	output collidingY, collidingX; //Whether or not to bounce the ball
	wire [9:0] currentPos;
	output [2:0] vecX, vecY;
	
	checkColliding collider (ballPosX, ballPosY, currentPos, clk_in, collidingY, vecX, vecY, reset, posX, collidingX);
	
	drawingPaddle paddleDrawer (currentPos, col, row, color);
	
	movePaddle mover (directionInput, currentPos, clk_in, reset);
	
//		always @(posedge clk_in) begin
//		if (directionInput[0] && currentPos > 1'b1)
//			currentPos <= currentPos - 1;
//		if (directionInput[1] && currentPos < 10'd564)
//			currentPos <= currentPos + 1;
//	end

endmodule

module checkColliding(ballPosX, ballPosY, currentPos, clk_in, collidingY, vecX, vecY, reset, posX, collidingX);
	input [9:0] ballPosX, currentPos;
	input clk_in, reset, posX;
	input [8:0] ballPosY;
	output reg collidingY, collidingX;
	reg [5:0] frameDelay;
	output reg [2:0] vecX, vecY;
	
	initial begin //default ball vector
		vecX <= 2'b01;
		vecY <= 2'b11;
	end
	
	always @(posedge clk_in) begin
		if(reset) begin
			vecX <= 2'b01;
			vecY <= 2'b11;
		end
		else begin
			//this section  handles the ball's motion vectors
			//The paddle is divided into 7 sections, each applying a motion vector to the ball
			if(ballPosX < currentPos + 7'd70 && (ballPosX > currentPos - 4'd10 || ballPosX > currentPos) && ballPosY > 9'd460 && frameDelay > 4'd15) begin
				if(ballPosX < currentPos + 4'd10) begin
					vecX <= 2'b11;
					vecY <= 2'b01;
					collidingX <= posX;
				end
				else if(ballPosX < currentPos + 5'd20) begin
					vecX <= 2'b10;
					vecY <= 2'b10;
					collidingX <= posX;
				end
				else if(ballPosX < currentPos + 5'd30) begin
					vecX <= 2'b01;
					vecY <= 2'b11;
					collidingX <= posX;
				end
				else if(ballPosX < currentPos + 6'd40) begin
					vecX <= vecX;
					vecY <= vecY;
				end
				else if(ballPosX < currentPos + 6'd50) begin
					vecX <= 2'b01;
					vecY <= 2'b11;
					collidingX <= posX;
				end
				else if(ballPosX < currentPos + 6'd60) begin
					vecX <= 2'b10;
					vecY <= 2'b10;
					collidingX <= posX;
				end
				else begin
					vecX <= 2'b11;
					vecY <= 2'b01;
					collidingX <= posX;
				end
				
				collidingY <= 1;
				frameDelay <= 0;
				end
			else begin
				//Here we have a delay so the ball can only "collide" again after at least 15 frames have passed
				//This is to prevent the ball repeatedly bouncing up and down and getting "stuck"
				collidingX <= 0;
				collidingY <= 0;
				frameDelay <= frameDelay < 16 ? frameDelay + 1 : frameDelay;
				end
		end
	end
endmodule

module drawingPaddle(currentPos, col, row, color);
	input [9:0] currentPos;
	input  [9:0]  col;	//0->639
	input  [8:0]  row;	//0->479
	output reg [2:0] color;
	
	always @* begin
		color <= (col < currentPos + 7'd70 && col > currentPos && row < 9'd479 && row > 9'd470) ? 3'b111 : 3'b000;
	end
endmodule

module movePaddle(directionInput, currentPos, clk_in, reset); //This just moves the paddle left or right depending on the button input every clock cycle
	input [1:0] directionInput;
	input clk_in, reset;
	output reg [9:0] currentPos;
	
	initial begin
		currentPos <= 10'd285;
	end

	always @(posedge clk_in) begin
	if (reset)
		currentPos <= 10'd285;
	else begin
		if (directionInput[0] && ~directionInput[1] && currentPos > 4'd1)
			currentPos = currentPos - 3'b100;
		if (~directionInput[0] && directionInput[1] && currentPos < 10'd569)
			currentPos = currentPos + 3'b100;
		end
	end
endmodule
